const express = require('express');
const cors = require('cors');
const routes = require('./routes');
require('dotenv').config();
const { PORT } = require('./config/index');
const { DetectIntent } = require('./helper/dialogflowCX');
const authMiddleware = require("./helper/basic-auth");
const config = require("./config");
const helmet = require('helmet');


const app = express();
app.use(express.json());
app.use(helmet());
const router = express.Router();
app.get('/healthcheck', (req, res) => {
    res.send('All Good');
});

app.use(cors());
let db;
router.use('/api/v1', routes(db));

if (config.auth.enable) {
    app.use(authMiddleware);
}

app.use('/', router);
app.listen(PORT, () => {
    console.log('Sever is up and running.');
})