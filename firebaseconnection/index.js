const firebaseAdmin = require('firebase-admin');
const getCredentials = function () {
    let credential;
    switch (process.env.NODE_ENV) {
        case 'development':
        case 'production' :
        case 'dev' :
        case 'qa' :
            credential = firebaseAdmin.credential.applicationDefault();
            break;
        default:
            const serviceAccount = require (process.env.SERVICE_ACCOUNT_JSON_FILE_PATH);
            credential = firebaseAdmin.credential.cert(serviceAccount);
            break;
    }
    return credential;
};
firebaseAdmin.initializeApp({
    credential: getCredentials()
});
module.exports = {
    firestoreDB: firebaseAdmin.firestore()
};