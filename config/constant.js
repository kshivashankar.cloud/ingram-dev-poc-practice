module.exports = {
    PORT: process.env.PORT || 3000,
    PROJECT_ID: process.env.PROJECT_ID,
    LOCATION: process.env.LOCATION,
    AGENT_ID: process.env.AGENT_ID,
    CONFIGURATION: {
        apiEndpoint: 'dialogflow.googleapis.com'
    },
    auth: {
        enable: true,
        username: process.env.USER_NAME,
        password: process.env.PASSWORD
    },
    firedb: {
        collections: process.env.COLLECTION,
        id: process.env.ID
    }
}
