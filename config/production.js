'use strict';

module.exports = {
    port: process.env.PORT || 443,
    auth: {
        enable: true,
        username: process.env.USER_NAME,
        password: process.env.PASSWORD
    },
    logger: {
        piiFields: []
    },
    projectId: process.env.PROJECT_ID
    
};
