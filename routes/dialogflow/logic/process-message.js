'use strict';

const { DetectIntent } = require('../../../helper/dialogflowCX');
const logger = require('../../../logger');
const firestoreDB = require("../../../firebaseconnection/index").firestoreDB;
const config = require('../../../config');
let data;
/**
 * Send a query to the dialogflow agent, and return the query result.
 * @param {*} db references to all the DBs used in the application
 * @param {Object} params
 * @param {String} params.languageCode  - Language Code sent by the UI
 * @param {String} params.message - Message sent by the user
 * @param {String} params.userId - Unique ID of the user
 * @param {String} params.customerId - Customer Id of the user
 * @param {Boolean} [params.isNewSession] - If new session has to be created for the given user
 * @param {Boolean} [params.isTest] - If the request is from test automation framework
 * @param {Object} [params.customParams] - Any custom parameters to be sent to Dialogflow in payload
 * @returns {Promise<any>} Detect Intent API response
 */
const processMessage = async (db, params) => {
    logger.log('info', `Detect Intent API Request Body for userId: ${params.userId}`, null, params);

    if (data != params.customerId) {
        const colRef = firestoreDB.collection(config.firedb.collections).doc(config.firedb.id);
        const updateObj = {
            CUSTOMER_ID: params.customerId
        }
        await colRef.update(updateObj);
    }

    const result = await DetectIntent(params);
    data = params.customerId;

    logger.log('info', `Dialogflow Response for userId: ${params.userId} & sessionId: ${params.sessionId}`, null, result);



    const queryResult = result.queryResult;
    logger.log('info', `Query: ${queryResult.queryText} for userId: ${params.userId} & sessionId: ${params.sessionId}`);
    logger.log('info', `Response: ${queryResult.fulfillmentText} for userId: ${params.userId} & sessionId: ${params.sessionId}`);
    if (queryResult.intent) {
        logger.log('info', `Intent: ${queryResult.intent.displayName} for userId: ${params.userId} & sessionId: ${params.sessionId}`, null);
    } else {
        logger.log('info', `No intent matched for userId: ${params.userId} & sessionId: ${params.sessionId}`, null);
    }
    logger.log('info', `Sending Response back to user ${params.userId} for sessionId: ${params.sessionId} from Detect Intent API`, null, result);

    return result;
};

module.exports = {
    processMessage: processMessage
};
