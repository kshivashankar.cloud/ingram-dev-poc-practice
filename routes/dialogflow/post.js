'use strict';

const ValidationError = require('../../helper/validation-error');
const Ajv = require('ajv');
const ajv = new Ajv({ allErrors: true });
const validate = ajv.compile(require('./schemas/detect-intent-request.json'));
const processMessage = require('./logic/process-message');
const logger = require('../../logger');

/**
 * This function is used to hit the detect intent API for the given user query
 * @param {*} db references to all the DBs used in the application
 * @returns {function(Request, Response, function(?Error))}
 */
module.exports = (db) => {
    /**
     * @param {Request} req HTTP request
     * @param {Response} res HTTP response
     * @param {function(?Error)} next Next Function calls the next middleware in order
     * @returns {Promise<void>}
     */
    return async (req, res, next) => {
        try {
            logger.log('info', 'Detect Intent API');

            const body = req.body;

            // Validate request
            if (!validate(body)) {
                logger.log('error', validate.errors);
                throw new ValidationError(validate.errors);
            }

            const response = await processMessage.processMessage(db, body);

            return res.status(200).send({ data: response, message: 'Detect Intent Response' });
        } catch (error) {
            logger.log('error', 'Error in Detect Intent API', null, error);
            next(error);
        }
    };
};
