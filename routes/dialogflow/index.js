'use strict';

const router = require('express').Router({ mergeParams: true });

/**
 * Routes defined for Dialogflow APIs
 * @param {*} db references to all the DBs used in the application
 * @returns {Router}
 */
module.exports = (db) => {
    router.post('/detect-intent', require('./post')(db));

    return router;
};
