'use strict';

const router = require('express').Router({ mergeParams: true });

/**
 * Definitions of all the routes
 * @param {*} db references to all the DBs used in the application
 * @returns {Router}
 */
module.exports = (db) => {
    router.use('/dialogflow', require('./dialogflow')(db));

    return router;
};
