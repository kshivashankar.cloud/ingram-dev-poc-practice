// Copyright 2020 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

'use strict';
const { SessionsClient } = require('@google-cloud/dialogflow-cx');
const { PROJECT_ID, LOCATION, AGENT_ID, CONFIGURATION } = require('../config/index');

const DetectIntent = async (params) => {

    /**
     * TODO(developer): Uncomment these variables before running the sample.
     */
    // const projectId = 'my-project';
    // const location = 'global';
    // const agentId = 'my-agent';
    // const query = 'Hello';
    // const languageCode = 'en'

    /**
     * Example for regional endpoint:
     *   const location = 'us-central1'
     *   const client = new SessionsClient({apiEndpoint: 'us-central1-dialogflow.googleapis.com'})
     */

    const client = new SessionsClient(CONFIGURATION);
    const sessionId = params.sessionId;
    const sessionPath = client.projectLocationAgentSessionPath(
        PROJECT_ID,
        LOCATION,
        AGENT_ID,
        sessionId
    );
    const request = {
        session: sessionPath,
        queryInput: {
            text: {
                text: params.message,
            },
            languageCode: params.languageCode,
            customerId: params.customerId
        },
    };
    console.log(request);
    const [response] = await client.detectIntent(request);
    console.log(`User Query: ${params.message}`);
    for (const message of response.queryResult.responseMessages) {
        if (message.text) {
            console.log(`Agent Response: ${message.text.text}`);
        }
    }
    if (response.queryResult.match.intent) {
        console.log(
            `Matched Intent: ${response.queryResult.match.intent.displayName}`
        );
    }
    console.log(
        `Current Page: ${response.queryResult.currentPage.displayName}`
    );
    return response;
}

module.exports = { DetectIntent }